From 84abefc61f51175c7bef2b97716c69b77cbb1da8 Mon Sep 17 00:00:00 2001
From: Jordan Petridis <jordan@centricular.com>
Date: Fri, 8 Dec 2023 13:29:49 +0200
Subject: [PATCH 1/6] components/linux.bst: Enable hwrandom for physical cpus

---
 elements/components/linux.bst | 15 ++++++++++++++-
 1 file changed, 14 insertions(+), 1 deletion(-)

diff --git a/elements/components/linux.bst b/elements/components/linux.bst
index 231e1330d..c1c6fbe8a 100644
--- a/elements/components/linux.bst
+++ b/elements/components/linux.bst
@@ -244,7 +244,6 @@ config:
 
     enable BLK_MQ_VIRTIO
     module VIRTIO_CONSOLE
-    enable HW_RANDOM_VIRTIO
     module VIRTIO_MMIO
     module CRYPTO_DEV_VIRTIO
 
@@ -1028,6 +1027,20 @@ config:
         ;;
     esac
 
+
+    # Enable HW Random
+    enable HW_RANDOM
+    enable HW_RANDOM_VIRTIO
+    case "%{arch}" in
+      x86_64|i686)
+        enable HW_RANDOM_AMD
+        enable HW_RANDOM_INTEL
+        ;;
+      aarch64|arm)
+        enable HW_RANDOM_ARM_SMCCC_TRNG
+        ;;
+    esac
+
     # I2C/SMBus
     case "%{arch}" in
       x86_64|i686)
-- 
2.43.0


From cb12a1c928de2cd81b3a57874cb3b7cd41bbf956 Mon Sep 17 00:00:00 2001
From: Jordan Petridis <jordan@centricular.com>
Date: Thu, 7 Dec 2023 21:26:36 +0200
Subject: [PATCH 2/6] components/linux.bst: Use UNWINDER_FRAME_POINTER instead
 of UNWINDER_ORC

---
 elements/components/linux.bst | 11 +++++++++++
 1 file changed, 11 insertions(+)

diff --git a/elements/components/linux.bst b/elements/components/linux.bst
index c1c6fbe8a..41caa78ad 100644
--- a/elements/components/linux.bst
+++ b/elements/components/linux.bst
@@ -156,6 +156,17 @@ config:
     # Userspace firmware loading not supported
     remove FW_LOADER_USER_HELPER
 
+    # UNWINDER_ORC is great for the kernel
+    # but it wasn't designed to be used from the userspace
+    # thus we need FRAME_POINTER so userspace applications
+    # will also be able to capture traces efficiently
+    case "%{arch}" in
+      x86_64|i686)
+        remove UNWINDER_ORC
+        enable UNWINDER_FRAME_POINTER
+      ;;
+    esac
+
     # Some udev/virtualization requires
     enable DMIID
 
-- 
2.43.0


From 3ced004ad112fa09810dfee31d0d0f780f0bdf24 Mon Sep 17 00:00:00 2001
From: Jordan Petridis <jordan@centricular.com>
Date: Sat, 9 Dec 2023 17:52:07 +0200
Subject: [PATCH 3/6] components/linux.bst: Enable IOMMUFD and IRQ_REMAP

---
 elements/components/linux.bst | 5 +++++
 1 file changed, 5 insertions(+)

diff --git a/elements/components/linux.bst b/elements/components/linux.bst
index 41caa78ad..ea9a604b6 100644
--- a/elements/components/linux.bst
+++ b/elements/components/linux.bst
@@ -1029,11 +1029,16 @@ config:
 
     # CPU support
     enable PINCTRL
+    if has IOMMU_SUPPORT; then
+      enable IOMMUFD
+    fi
     case "%{arch}" in
       x86_64|i686)
         # pinctrl_amd might not work as module if it is loaded to late
         enable PINCTRL_AMD
         enable AMD_IOMMU_V2
+        enable INTEL_IOMMU
+        enable IRQ_REMAP
         enable X86_AMD_PLATFORM_DEVICE
         ;;
     esac
-- 
2.43.0


From 760083602a99f3bdbe7b6f2cdc407f1f0aeb079c Mon Sep 17 00:00:00 2001
From: Jordan Petridis <jordan@centricular.com>
Date: Sat, 9 Dec 2023 17:57:03 +0200
Subject: [PATCH 4/6] components/linux.bst: Explicitly enable PERF_EVENTS

Currently only the _INTEL and _AMD_UNCORE are enabled in the default
config, and since we are here, explicitly enable all of them so they
won't go missing without us noticing.
---
 elements/components/linux.bst | 12 ++++++++++++
 1 file changed, 12 insertions(+)

diff --git a/elements/components/linux.bst b/elements/components/linux.bst
index ea9a604b6..88fa21e28 100644
--- a/elements/components/linux.bst
+++ b/elements/components/linux.bst
@@ -1044,6 +1044,18 @@ config:
     esac
 
 
+    # Performance monitoring
+    case "%{arch}" in
+      x86_64|i686)
+        enable PERF_EVENTS_AMD_BRS
+        enable PERF_EVENTS_INTEL_CSTATE
+        enable PERF_EVENTS_INTEL_RAPL
+        enable PERF_EVENTS_INTEL_UNCORE
+        module PERF_EVENTS_AMD_POWER
+        module PERF_EVENTS_AMD_UNCORE
+      ;;
+    esac
+
     # Enable HW Random
     enable HW_RANDOM
     enable HW_RANDOM_VIRTIO
-- 
2.43.0


From e764f052bc86ea91cf9416546711d82a335dac57 Mon Sep 17 00:00:00 2001
From: Jordan Petridis <jordan@centricular.com>
Date: Sat, 9 Dec 2023 18:24:18 +0200
Subject: [PATCH 5/6] components/linux.bst: enable shadow stack support when
 possible

---
 elements/components/linux.bst | 4 ++++
 1 file changed, 4 insertions(+)

diff --git a/elements/components/linux.bst b/elements/components/linux.bst
index 88fa21e28..22a5fd0ad 100644
--- a/elements/components/linux.bst
+++ b/elements/components/linux.bst
@@ -1040,9 +1040,13 @@ config:
         enable INTEL_IOMMU
         enable IRQ_REMAP
         enable X86_AMD_PLATFORM_DEVICE
+        enable CONFIG_X86_USER_SHADOW_STACK
         ;;
     esac
 
+    if has ARCH_SUPPORTS_SHADOW_CALL_STACK; then
+      enable SHADOW_CALL_STACK
+    fi
 
     # Performance monitoring
     case "%{arch}" in
-- 
2.43.0


From a368ecf403bd9ac5a6afcf3e4a0b788a6fd8e345 Mon Sep 17 00:00:00 2001
From: Jordan Petridis <jordan@centricular.com>
Date: Sat, 9 Dec 2023 19:09:55 +0200
Subject: [PATCH 6/6] components/linux.bst: Enable IRQ_TIME_ACCOUNTING

---
 elements/components/linux.bst | 4 ++++
 1 file changed, 4 insertions(+)

diff --git a/elements/components/linux.bst b/elements/components/linux.bst
index 22a5fd0ad..84ce0875b 100644
--- a/elements/components/linux.bst
+++ b/elements/components/linux.bst
@@ -1048,6 +1048,10 @@ config:
       enable SHADOW_CALL_STACK
     fi
 
+    if has HAVE_IRQ_TIME_ACCOUNTING; then
+      enable IRQ_TIME_ACCOUNTING
+    fi
+
     # Performance monitoring
     case "%{arch}" in
       x86_64|i686)
-- 
2.43.0

